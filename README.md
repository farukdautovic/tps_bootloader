# README #

TPS Bootloader - STM32F103RB processor

### What is this repository for? ###

Bootloader for TPS Device
Version 1.0.0

### How do I get set up? ###

Developed in Atolic True Studio
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact