/**
 * @file flash.c
 * @brief Module for flash memory related functions - implementation
 *
 * @par Copyright (c) 2019 Microwee.
 *
 */

#include "flash.h"
#include <stdint.h>

/* ==========================================================================
    Data type definition
   ========================================================================== */

/* Function pointer for jumping to user application. */
typedef void (*pFunction)(void);

/* ==========================================================================
    Public function definition
   ========================================================================== */

/** see header file */
flash_status flash_erase(uint32_t address)
{
  HAL_FLASH_Unlock();

  flash_status status = FLASH_ERROR;
  FLASH_EraseInitTypeDef erase_init;
  uint32_t error = 0u;

  erase_init.TypeErase = FLASH_TYPEERASE_PAGES;
  erase_init.PageAddress = address;
  erase_init.Banks = FLASH_BANK_1;
  /* Calculate the number of pages from "address" and the end of flash. */
  erase_init.NbPages = (FLASH_BANK1_END - address) / FLASH_PAGE_SIZE;
  /* Do the actual erasing. */
  if (HAL_OK == HAL_FLASHEx_Erase(&erase_init, &error))
  {
    status = FLASH_OK;
  }

  HAL_FLASH_Lock();

  return status;
}

/** see header file */
flash_status flash_write(uint32_t address, uint32_t *data, uint32_t length)
{
  flash_status status = FLASH_OK;

  HAL_FLASH_Unlock();

  /* Loop through the array. */
  for (uint32_t i = 0u; (i < length) && (FLASH_OK == status); i++)
  {
    /* If we reached the end of the memory, then report an error and don't do anything else.*/
    if (FLASH_APP_END_ADDRESS <= address)
    {
      status |= FLASH_ERROR_SIZE;
    }
    else
    {
      /* The actual flashing. If there is an error, then report it. */
      if (HAL_OK != HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address, data[i]))
      {
        status |= FLASH_ERROR_WRITE;
      }
      /* Read back the content of the memory. If it is wrong, then report an error. */
      if (((data[i])) != (*(volatile uint32_t*)address))
      {
        status |= FLASH_ERROR_READBACK;
      }

      /* Shift the address by a word. */
      address += 4u;
    }
  }

  HAL_FLASH_Lock();

  return status;
}

/* see header file */
flash_protection_t flash_get_protection_status(void)
{
  flash_protection_t flash_protection_type = fpt_PROTECTION_NONE;

  FLASH_OBProgramInitTypeDef ob_config = {0};

  HAL_FLASH_Unlock();

  /* Read current state of Option Control Register */
  HAL_FLASHEx_OBGetConfig(&ob_config);

  // TODO:: Figure out which flash area needs to be protected
  /* WRP Area */
  if (ob_config.WRPPage == OB_WRP_PAGES4TO7)
  {
    flash_protection_type |= fpt_PROTECTION_WRP;
  }


  /** RDP **/
  if(ob_config.RDPLevel != OB_RDP_LEVEL_0)
  {
    flash_protection_type |= fpt_PROTECTION_RDP;
  }

  HAL_FLASH_Lock();

  return flash_protection_type;
}

/* see header file */
flash_status flash_config_protection(uint32_t protection)
{
  FLASH_OBProgramInitTypeDef ob_config;
  HAL_StatusTypeDef status = HAL_ERROR;

   status = HAL_FLASH_Unlock();
   status |= HAL_FLASH_OB_Unlock();

  /* Set configured protection  */

  if (protection & fpt_PROTECTION_WRP)
  {
    ob_config.WRPState = OB_WRPSTATE_ENABLE;
  }
  else if (protection & fpt_PROTECTION_RDP)
  {
    ob_config.RDPLevel = OB_RDP_LEVEL_1;
  }
  else
  {
    ob_config.WRPState = OB_WRPSTATE_DISABLE;
  }

  status |= HAL_FLASHEx_OBProgram(&ob_config);

  /* Launch Option byte write - this generates a system reset. */
  /* The Option Control register will be locked automatically */
  HAL_FLASH_OB_Launch();

  status |= HAL_FLASH_OB_Lock();
  status |= HAL_FLASH_Lock();

  return (status == HAL_OK) ? FLASH_OK : FLASH_ERROR;
}

/* see header file */
void flash_jump_to_app(void)
{
  // Application vector table variables
  pFunction app_entry;
  uint32_t app_stack;

  /* Get the application stack pointer (First entry in the application vector table) */
  app_stack = (uint32_t) *((__IO uint32_t*)APPLICATION_ADDRESS);

  /* Get the application entry point (Second entry in the application vector table) */
  app_entry = (pFunction) *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);

  /* Reconfigure vector table offset register to match the application location */
  SCB->VTOR = APPLICATION_ADDRESS;

  HAL_DeInit();

  /* Set the application stack pointer */
  __set_MSP(app_stack);

  /* Start the application */
  app_entry();
}
