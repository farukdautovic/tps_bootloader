/**
 * @file    fw_updater.h
 * @brief   This module is responsible for updating firmware for the application - implementation
 *
 * @par Copyright (c) 2019 Microwee.
 */

#include "fw_updater.h"
#include <stdbool.h>
#include "flash.h"
#include "uart.h"
#include "bl_opts.h"


/* ==========================================================================
    Private constant and variable definition
   ========================================================================== */

static uint8_t fw_updater_packet_number = 1u;         /**< Packet number counter. */
static uint32_t fw_updater_actual_flash_address = 0u; /**< Address where we have to write. */
static uint8_t fw_updater_first_packet_received = false;   /**< First packet or not. */

/* ==========================================================================
    Private function prototypes
   ========================================================================== */

static fw_updater_status_t fw_updater_error_handler(uint8_t *error_number, uint8_t max_error_number);
static uint16_t fw_updater_calc_crc(uint8_t *data, uint16_t length);
static fw_updater_status_t xmodem_handle_packet(uint8_t size);
static fw_updater_status_t fw_updater_check_app_size(uint32_t app_size);


/* ==========================================================================
    Public function definition
   ========================================================================== */

/** see header file */
void fw_updater_receive(void)
{
  volatile fw_updater_status_t status = fust_OK;
  uint8_t error_number = 0u;

  fw_updater_first_packet_received = false;
  fw_updater_packet_number = 1u;
  fw_updater_actual_flash_address = APPLICATION_ADDRESS;

  /* Loop until there isn't any error (or until we jump to the user application). */
  while (fust_OK == status)
  {
    uint8_t header = 0x00u;

    /* Get the header from UART. */
    uart_status comm_status = uart_receive(&header, 1u);

    /* Spam the host (until we receive something) with ACSII "C", to notify it, we want to use CRC-16. */
    if ((UART_OK != comm_status) && (false == fw_updater_first_packet_received))
    {
      (void)uart_transmit_ch(X_C);
    }
    /* Uart timeout or any other errors. */
    else if ((UART_OK != comm_status) && (true == fw_updater_first_packet_received))
    {
      status = fw_updater_error_handler(&error_number, X_MAX_ERRORS);
    }
    else
    {
      /* Do nothing. */
    }

    /* The header can be: SOH, STX, EOT and CAN. */
    switch(header)
    {
      fw_updater_status_t packet_status = fust_ERROR;
      /* 128 or 1024 bytes of data. */
      case X_SOH:
      case X_STX:
        /* If the handling was successful, then send an ACK. */
        packet_status = xmodem_handle_packet(header);
        if (fust_OK == packet_status)
        {
          (void)uart_transmit_ch(X_ACK);
        }
        /* If the error was flash related, then immediately set the error counter to max (graceful abort). */
        else if (fust_ERROR_FLASH == packet_status)
        {
          error_number = X_MAX_ERRORS;
          status = fw_updater_error_handler(&error_number, X_MAX_ERRORS);
        }
        /* Error while processing the packet, either send a NAK or do graceful abort. */
        else
        {
          status = fw_updater_error_handler(&error_number, X_MAX_ERRORS);
        }
        break;
      /* End of Transmission. */
      case X_EOT:
        /* ACK, feedback to user (as a text), then jump to user application. */
        (void)uart_transmit_ch(X_ACK);
        (void)uart_transmit_str((uint8_t*)"\n\rFirmware updated!\n\r");

        /* Firmware updated, reset flag. */
        HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, BOOTLOADER_COMMAND_CLEAR);
        //FW_UPDATE_REQUIRED = BOOTLOADER_COMMAND_CLEAR;

        /* Enable flash write protection */
#if (FLASH_WRITE_PROTECTION)
        (void)uart_transmit_str((uint8_t*)"Enabling flash write protection and generating system reset...");
        if(flash_config_protection(fpt_PROTECTION_WRP) != FLASH_OK)
        {
          (void)uart_transmit_str((uint8_t*)"Failed to enable write protection.");
          (void)uart_transmit_str((uint8_t*)"Exiting Bootloader.");
        }
#endif

        (void)uart_transmit_str((uint8_t*)"Jumping to user application...\n\r");
        flash_jump_to_app();
        break;
      /* Abort from host. */
      case X_CAN:
        status = fust_ERROR;
        break;
      default:
        /* Wrong header. */
        if (UART_OK == comm_status)
        {
          status = fw_updater_error_handler(&error_number, X_MAX_ERRORS);
        }
        break;
    }
  }

}

/* ==========================================================================
    Private function definition
   ========================================================================== */

/**
 * @brief   Handles the fw_updater error.
 *          Raises the error counter, then if the number of the errors reached critical,
 *          do a graceful abort, otherwise send a NAK.
 * @param   *error_number:    Number of current errors (passed as a pointer).
 * @param   max_error_number: Maximal allowed number of errors.
 * @return  status: fust_ERROR in case of too many errors, fust_OK otherwise.
 */
static fw_updater_status_t fw_updater_error_handler(uint8_t *error_number, uint8_t max_error_number)
{
  fw_updater_status_t status = fust_OK;
  /* Raise the error counter. */
  (*error_number)++;
  /* If the counter reached the max value, then abort. */
  if ((*error_number) >= max_error_number)
  {
    /* Graceful abort. */
    (void)uart_transmit_ch(X_CAN);
    (void)uart_transmit_ch(X_CAN);
    status = fust_ERROR;
  }
  /* Otherwise send a NAK for a repeat. */
  else
  {
    (void)uart_transmit_ch(X_NAK);
    status = fust_OK;
  }
  return status;
}

/**
 * @brief   Calculates the CRC-16 for the input package.
 * @param   *data:  Array of the data which we want to calculate.
 * @param   length: Size of the data, either 128 or 1024 bytes.
 * @return  status: The calculated CRC.
 */
static uint16_t fw_updater_calc_crc(uint8_t *data, uint16_t length)
{
    uint16_t crc = 0u;
    while (length)
    {
        length--;
        crc = crc ^ ((uint16_t)*data++ << 8u);
        for (uint8_t i = 0u; i < 8u; i++)
        {
            if (crc & 0x8000u)
            {
                crc = (crc << 1u) ^ 0x1021u;
            }
            else
            {
                crc = crc << 1u;
            }
        }
    }
    return crc;
}

/**
 * @brief   This function handles the data packet we get from the xmodem protocol.
 * @param   header: SOH or STX.
 * @return  status: Report about the packet.
 */
static fw_updater_status_t xmodem_handle_packet(uint8_t header)
{
  fw_updater_status_t status = fust_OK;
  uint16_t size = 0u;
  if (X_SOH == header)
  {
    size = X_PACKET_128_SIZE;
  }
  else if (X_STX == header)
  {
    size = X_PACKET_1024_SIZE;
  }
  else
  {
    /* Wrong header type. */
    status = fust_ERROR;
  }
  uint16_t length = size + X_PACKET_DATA_INDEX + X_PACKET_CRC_SIZE;
  uint8_t received_data[X_PACKET_1024_SIZE + X_PACKET_DATA_INDEX + X_PACKET_CRC_SIZE];

  /* Get the packet (except for the header) from UART. */
  uart_status comm_status = uart_receive(&received_data[0u], length);
  /* The last two bytes are the CRC from the host. */
  uint16_t crc_received = ((uint16_t)received_data[length-2u] << 8u) | ((uint16_t)received_data[length-1u]);
  /* We calculate it too. */
  uint16_t crc_calculated = fw_updater_calc_crc(&received_data[X_PACKET_DATA_INDEX], size);

  /* If it is the first packet, then erase the memory. */
  if (false == fw_updater_first_packet_received)
  {
    if (FLASH_OK == flash_erase(APPLICATION_ADDRESS))
    {
      fw_updater_first_packet_received = true;
    }
    else
    {
      status |= fust_ERROR_FLASH;
    }
  }

  /* Error handling and flashing. */
  if (fust_OK == status)
  {
    if (UART_OK != comm_status)
    {
      /* UART error. */
      status |= fust_ERROR_UART;
    }
    if (fw_updater_packet_number != received_data[X_PACKET_NUMBER_INDEX])
    {
      /* Packet number counter mismatch. */
      status |= fust_ERROR_NUMBER;
    }
    if (255u != (received_data[X_PACKET_NUMBER_INDEX] +  received_data[X_PACKET_NUMBER_COMPLEMENT_INDEX]))
    {
      /* The sum of the packet number and packet number complement aren't 255. */
      /* The sum always has to be 255. */
      status |= fust_ERROR_NUMBER;
    }
    if (crc_calculated != crc_received)
    {
      /* The calculated and received CRC are different. */
      status |= fust_ERROR_CRC;
    }
    /* Do the actual flashing. */
    if (FLASH_OK != flash_write(fw_updater_actual_flash_address, (uint32_t*)&received_data[X_PACKET_DATA_INDEX], (uint32_t)size/4u))
    {
      /* Flashing error. */
      status |= fust_ERROR_FLASH;
    }
  }

  /* Raise the packet number and the address counters (if there wasn't any error). */
  if (fust_OK == status)
  {
    fw_updater_packet_number++;
    fw_updater_actual_flash_address += size;
  }

  return status;
}

/**
 * @brief   This function checks whether APP for firmware can fit into dedicated app space
 * @param   app_size: Size of app to receive in bytes
 * @return  status: Report about the packet.
 */
static fw_updater_status_t fw_updater_check_app_size(uint32_t app_size)
{
  return ((FLASH_BASE + FLASH_SIZE - APPLICATION_ADDRESS) >= app_size) ? fust_OK : fust_ERROR;
}
