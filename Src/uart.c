/**
 * @file    uart.c
 * @brief   This module is a layer between the HAL UART functions and application - implementation
 *
 * @par Copyright (c) 2019 Microwee.
 */

#include "uart.h"

/* ==========================================================================
    Public function definition
   ========================================================================== */

/** see header file */
uart_status uart_receive(uint8_t *data, uint16_t length)
{
  uart_status status = UART_ERROR;

  if (HAL_OK == HAL_UART_Receive(&huart1, data, length, UART_TIMEOUT))
  {
    status = UART_OK;
  }

  return status;
}

/** see header file */
uart_status uart_transmit_str(uint8_t *data)
{
  uart_status status = UART_ERROR;
  uint16_t length = 0u;

  /* Calculate the length. */
  while ('\0' != data[length])
  {
    length++;
  }

  if (HAL_OK == HAL_UART_Transmit(&huart1, data, length, UART_TIMEOUT))
  {
    status = UART_OK;
  }

  return status;
}

/** see header file */
uart_status uart_transmit_ch(uint8_t data)
{
  uart_status status = UART_ERROR;

  /* Make available the UART module. */
  if (HAL_UART_STATE_TIMEOUT == HAL_UART_GetState(&huart1))
  {
    HAL_UART_Abort(&huart1);
  }

  if (HAL_OK == HAL_UART_Transmit(&huart1, &data, 1u, UART_TIMEOUT))
  {
    status = UART_OK;
  }
  return status;
}
