/**
 * @file    fw_updater.h
 * @brief   This module is responsible for updating firmware for the application - interface
 *
 * @par Copyright (c) 2019 Microwee.
 */

#ifndef FW_UPDATER_H_
#define FW_UPDATER_H_

#include "stm32f1xx_hal.h" // For accessing STM processor

#include <stdint.h>

/* ==========================================================================
    Macro definition
   ========================================================================== */
/* Bytes defined by the protocol. */
#define X_SOH ((uint8_t)0x01u)  /**< Start Of Header (128 bytes). */
#define X_STX ((uint8_t)0x02u)  /**< Start Of Header (1024 bytes). */
#define X_EOT ((uint8_t)0x04u)  /**< End Of Transmission. */
#define X_ACK ((uint8_t)0x06u)  /**< Acknowledge. */
#define X_NAK ((uint8_t)0x15u)  /**< Not Acknowledge. */
#define X_CAN ((uint8_t)0x18u)  /**< Cancel. */
#define X_C   ((uint8_t)0x43u)  /**< ASCII "C", to notify the host, we want to use CRC16. */

/* Maximum allowed errors (user defined). */
#define X_MAX_ERRORS ((uint8_t)3u)

/* Sizes of the packets. */
#define X_PACKET_128_SIZE   ((uint16_t)128u)
#define X_PACKET_1024_SIZE  ((uint16_t)1024u)
#define X_PACKET_CRC_SIZE   ((uint16_t)2u)

/* Relative (not counting the header) location of the packets. */
#define X_PACKET_NUMBER_INDEX             ((uint16_t)0u)
#define X_PACKET_NUMBER_COMPLEMENT_INDEX  ((uint16_t)1u)
#define X_PACKET_DATA_INDEX               ((uint16_t)2u)

/* RTC Backup register used to store information about fw update status */
//#define FW_UPDATE_REQUIRED (RTC->BKP_DR1_D)

//Bootloader Commands
#define BOOTLOADER_COMMAND_CLEAR    (0)
#define BOOTLOADER_REQUEST          (0x1)

/* ==========================================================================
    Data type definition
   ========================================================================== */

extern RTC_HandleTypeDef hrtc;

/* Status report for the functions. */
typedef enum {
  fust_OK            = 0x00u, /**< The action was successful. */
  fust_ERROR_CRC     = 0x01u, /**< CRC calculation error. */
  fust_ERROR_NUMBER  = 0x02u, /**< Packet number mismatch error. */
  fust_ERROR_UART    = 0x04u, /**< UART communication error. */
  fust_ERROR_FLASH   = 0x06u, /**< Flash related error. */
  fust_ERROR         = 0xFFu  /**< Generic error. */
} fw_updater_status_t;

/* ==========================================================================
 *  Public function prototypes
 * ========================================================================== */

/**
 * @brief   This function is the base of the Firmware updater.
 *          When we receive a header from UART, it decides what action it shall take.
 * @param   void
 * @return  void
 */
void fw_updater_receive(void);

#endif /* FW_UPDATER_H_ */
