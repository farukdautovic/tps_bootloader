/**
 * @file    uart.h
 * @brief   This module is a layer between the HAL UART functions and application - interface
 *
 * @par Copyright (c) 2019 Microwee.
 */

#ifndef UART_H_
#define UART_H_

#include "stm32f1xx_hal.h"

/* ==========================================================================
    Data type definition
   ========================================================================== */

extern UART_HandleTypeDef huart1;

/* Timeout for HAL. */
#define UART_TIMEOUT ((uint16_t)1000u)

/* Status report for the functions. */
typedef enum {
  UART_OK     = 0x00u, /**< The action was successful. */
  UART_ERROR  = 0xFFu  /**< Generic error. */
} uart_status;

/* ==========================================================================
 *  Public function prototypes
 * ========================================================================== */

/**
 * @brief   Receives data from UART.
 * @param   *data: Array to save the received data.
 * @param   length:  Size of the data.
 * @return  status: Report about the success of the receiving.
 */
uart_status uart_receive(uint8_t *data, uint16_t length);

/**
 * @brief   Transmits a string to UART.
 * @param   *data: Array of the data.
 * @return  status: Report about the success of the transmission.
 */
uart_status uart_transmit_str(uint8_t *data);

/**
 * @brief   Transmits a single char to UART.
 * @param   *data: The char.
 * @return  status: Report about the success of the transmission.
 */
uart_status uart_transmit_ch(uint8_t data);


#endif /* UART_H_ */
