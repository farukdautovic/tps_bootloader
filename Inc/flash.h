/**
 * @file flash.h
 * @brief Module for flash memory related functions - interface
 *
 * @par Copyright (c) 2019 Microwee.
 *
 */

#ifndef FLASH_H_
#define FLASH_H_

#include "stm32f1xx_hal.h" // For accessing STM processor

/* ==========================================================================
    Public constant and variable definition
   ========================================================================== */

/* Application start address  - Bootloader size 8kB = 0x2000*/
#define APPLICATION_ADDRESS        (0x08002000)
#define FLASH_APP_END_ADDRESS   ((uint32_t)FLASH_BANK1_END-0x10u) // Leave a little extra space at the end.

/* ==========================================================================
    Data type definition
   ========================================================================== */

/* Status report for the functions. */
typedef enum {
  FLASH_OK              = 0x00u, /**< The action was successful. */
  FLASH_ERROR_SIZE      = 0x01u, /**< The binary is too big. */
  FLASH_ERROR_WRITE     = 0x02u, /**< Writing failed. */
  FLASH_ERROR_READBACK  = 0x04u, /**< Writing was successful, but the content of the memory is wrong. */
  FLASH_ERROR           = 0xFFu  /**< Generic error. */
} flash_status;

/* Flash Protection Types */
typedef enum
{
  fpt_PROTECTION_NONE  = 0,   ///< Protection disabled
  fpt_PROTECTION_WRP   = 0x1, ///< Write protection enabled
  fpt_PROTECTION_RDP   = 0x2, ///< Read protection enabled
} flash_protection_t;

/* ==========================================================================
 *  Public function prototypes
 * ========================================================================== */

/**
 * @brief   This function erases the memory.
 * @param   address: First address to be erased (the last is the end of the flash).
 * @return  status: Report about the success of the erasing.
 */
flash_status flash_erase(uint32_t address);

/**
 * @brief   This function flashes the memory.
 * @param   address: First address to be written to.
 * @param   *data:   Array of the data that we want to write.
 * @param   *length: Size of the array.
 * @return  status: Report about the success of the writing.
 */
flash_status flash_write(uint32_t address, uint32_t *data, uint32_t length);

/**
 * @brief   This function gets flash protection status.
 * @return  status: Report about the success of the flash operation.
 */
flash_protection_t flash_get_protection_status(void);

/**
 * @brief   This function configures flash protection.
 * @param   protection ... type of protection to configure
 * @return  status: Report about the success of the flash operation.
 */
flash_status flash_config_protection(uint32_t protection);

/**
 * @brief Function for jumping to application from bootloader
 */
void flash_jump_to_app(void);

#endif /* FLASH_H_ */
